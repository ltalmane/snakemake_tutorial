import os
#import numpy as np
import math

configfile : "config.yaml"

include : "rules/plot_hist_simple.smk"
#include : "rules/plot_hist_wild.smk"
#include : "rules/plot_hist_wild_error1.smk"
#include : "rules/plot_hist_wild_error2.smk"
#include : "rules/plot_hist_wild_error3.smk"
#include : "rules/plot_hist_wild_error4.smk"

rule all:
    input:
        pdf = "plots/dataset1.hg38.hists.50.pdf"

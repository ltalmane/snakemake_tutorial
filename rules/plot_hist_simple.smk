ruleorder : make_regions_whitelist > make_regions

rule sort_file:
    input:
        "data/CTCF_chip.hg38.bed"
    output:
        "data/CTCF_chip.hg38.sorted.bed"
    shell:
        "sort {input} > {output} "

rule make_regions_whitelist:
    input:
        chip = "data/CTCF_chip.hg38.sorted.bed",
        blacklist = config["blacklist_hg38"]
    output:
        regions = "data/CTCF_chip.hg38.sorted.regions.bed"
    shell:
        "bedtools intersect -v -a {input.chip} -b {input.blacklist} | cut -f 1,2,3 > {output.regions} "

rule make_regions:
    input:
        chip = "data/CTCF_chip.hg38.sorted.bed"
    output:
        regions = "data/CTCF_chip.hg38.sorted.regions.bed"
    shell:
        "cut -f 1,2,3  {input.chip} > {output.regions}"

rule plot_flank_len:
    input:
        regions = "data/CTCF_chip.hg38.sorted.regions.bed"
    output:
        flankreg = temp("data/CTCF_chip.hg38.sorted.regions.flank.bed"),
        pdf = "plots/CTCF_chip.hg38.sorted.regions.flank.hist.pdf",
        txt = "data/CTCF_chip.hg38.sorted.regions.flank.hist.txt"
    params:
        flank = config["flankreg"],
        bins = 50
    shell:
        " awk '{{FS=OFS=\"\\t\" ; print $1,$2-{params.flank},$3+{params.flank} }}' {input.regions} > {output.flankreg} ;"
        " Rscript scripts/plot_hist.R -f {output.flankreg} -p {output.pdf} -t {output.txt} -b {params.bins}"

rule swapsies:
    input:
        txt = rules.plot_flank_len.output.txt
    output:
        swapped = "data/CTCF_chip.hg38.sorted.regions.flank.hist.swapped.txt"
    run:
        with open(input.txt) as file, open(output.swapped,"w") as outfile:
                regs = [[x[1],x[0]] for x in map(lambda s : s.rstrip().split("\t"),file.readlines())]
                for i in regs:
                    outfile.write("\t".join(i) + "\n")

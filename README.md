This is a dummy project for the Snakemake tutorial. 

There is an environment.yaml file on the top level to create a conda environment that installs necessary dependencies for this workflow to run, as well as Snakemake itself.

Environment can be created with:

    conda env create -f environment.yaml

It can be then activated with:

    conda activate dummy
    
The workflow is designed to be run from within it.



-----------
---------------------------


There are several .smk rule files includeed in the Snakefile, some of which are commented out. 

The first one (rules/plot_hist_simple.smk), which is uncommented, is the simplest example to start with, and we will go through it first to get idea of basic concepts. 

Second one (rules/plot_hist_wild.smk) is more generalisable one, and runs essentially the same pipeline, but uses wildcards.

Last 4 that have an "error" suffix are examples of pipelines that have errors in them for you to work out.


-------
-------
*Commands to try:*

**Simple rule test:**

*(when `rules/plot_hist_simple.smk` in included into the Snakefile)*

`Snakemake –npr data/CTCF_chip.hg38.sorted.bed`

`Snakemake –npr data/CTCF_chip.hg38.sorted.regions.bed`

modify name of the "data/blacklist.hg38.bed" and do  `Snakemake –npr data/CTCF_chip.hg38.sorted.regions.bed` --> should apply a different rule from the last one

`Snakemake –npr plots/CTCF_chip.hg38.sorted.regions.flank.hist.pdf `

modify the flank in the config file and do  `Snakemake –npr plots/CTCF_chip.hg38.sorted.regions.flank.hist.pdf ` --> will use a different flank parameter

`Snakemake –npr data/CTCF_chip.hg38.sorted.regions.flank.swapped.txt`


**Wildcards rule test:**

*(when `rules/plot_hist_wild.smk` in included into the Snakefile)*

`Snakemake –npr data/CTCF_chip.hg38.sorted.bed`

`Snakemake –npr data/SP1_chip.mm10.sorted.bed` ---> will infer the wildcards

`Snakemake –npr data/CTCF_chip.hg38.sorted.regions.bed` --> will apply rule with blacklist filtering
    
`Snakemake –npr data/SP1_chip.mm10.sorted.regions.bed` --> will apply rule without blacklist filtering

**Use of functions and expand():**

`Snakemake -npr plots/dataset1.hg38.hists.pdf` --> will apply the rules to all the TFs in the dataset1.txt file

`Snakemake -npr plots/dataset1.hg38.hists.20.pdf` 

`Snakemake -npr plots/dataset1.hg38.hists.50.pdf` --> will use whatever number of bins you specify in the output target file

**Execution of the workflow with the target:**

`snakemake -npr` --> will execute the very first rule in the Snakefile

`snakemake --dag | dot -y -Tpng > dag.png`

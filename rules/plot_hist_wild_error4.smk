def getFilenames(filename):
    with open(filename) as file:
            names = map(lambda x : x.rstrip(),  file.readlines())
            return(names)


ruleorder : make_regions_whitelist > make_regions

wildcard_constraints:
    assembly = "hg38|hg19|mm9|mm10"

rule sort_file:
    input:
        "data/{filename}.{assembly}.bed"
    output:
        "data/{filename}.{assembly}.sorted.bed"
    shell:
        "sort {input} > {output} "

rule make_regions_whitelist:
    input:
        chip = "data/{filename}.{assembly}.sorted.bed",
        blacklist = lambda wildcards : config["blacklist_" + wildcards.assembly]
    output:
        "data/{filename}.{assembly}.sorted.regions.bed"
    shell:
        "bedtools intersect -v -a {input.chip} -b {input.blacklist} | cut -f 1,2,3 > {output.regions} "

rule make_regions:
    input:
        chip = "data/{filename}.{assembly}.sorted.bed"
    output:
        regions = "data/{filename}.{assembly}.sorted.regions.bed"
    shell:
        "cut -f 1,2,3 {input.chip} > {output.regions}"

rule plot_flank_len:
    input:
        regions = "data/{filename}.{assembly}.sorted.regions.bed"
    output:
        flankreg = temp("data/{filename}.{assembly}.sorted.regions.flank.bed"),
        pdf = "plots/{filename}.{assembly}.sorted.regions.flank.hist.pdf",
        txt = "data/{filename}.{assembly}.sorted.regions.flank.hist.txt"
    params:
        flank = config["flankreg"],
        bins = 50
    #threads : 1
    #conda : "environment.yaml"
    log : "tmp/test_log.log"
    shell:
        " awk '{{FS=OFS=\"\\t\" ; print $1,$2-{params.flank},$3+{params.flank} }}' {input.regions} > {output.flankreg} ;"
        " Rscript scripts/plot_hist.R -f {output.flankreg} -p {output.pdf} -t {output.txt} -b {params.bins}"

rule swapsies:
    input:
        txt = rules.plot_flank_len.output.txt
    output:
        swapped = "data/{filename}.{assembly}.sorted.regions.flank.hist.swapped.txt"
    run:
        with open(input.txt) as file, open(output.swapped,"w") as outfile:
                regs = [[x[1],x[0]] for x in map(lambda s : s.rstrip().split("\t"),file.readlines())]
                for i in regs:
                    outfile.write("\t".join(i) + "\n")

rule make_all_files:
    input:
        files = lambda wildcards : expand("data/{filename}.{assembly}.sorted.regions.flank.bed",filename = getFilenames("data/" + wildcards.dataset + ".txt"),assembly = wildcards.assembly)
    output:
        pdf = "plots/{dataset}.{assembly}.hists.pdf"
    params:
        #files = lambda wildcards : ",".join(expand("data/{filename}.{assembly}.sorted.regions.flank.bed",filename = getFilenames("data/" + wildcards.dataset + ".txt"),assembly = wildcards.assembly))
        files = lambda wildcards, input : ",".join(input.files)
    shell:
        "Rscripts scripts/plot_hist.R -f {params.files} -p {output.pdf}"

rule make_all_files_params:
    input:
        files = lambda wildcards : expand("data/{filename}.{assembly}.sorted.regions.flank.bed",filename = getFilenames("data/" + wildcards.dataset + ".txt"),assembly = wildcards.assembly)
    output:
        pdf = "plots/{dataset}.{assembly}.hists.{bins}.pdf",
        #txt = "tmp/{dataset}.{assembly}.hists.{bins}.txt"
    params:
        #files = lambda wildcards : ",".join(expand("data/{filename}.{assembly}.sorted.regions.flank.bed",filename = getFilenames("data/" + wildcards.dataset + ".txt"),assembly = wildcards.assembly))
        files = lambda wildcards, input : ",".join(input.files)
    wildcard_constraints:
        bins = "\d+"
    shell:
        "Rscript scripts/plot_hist.R -f {params.files} -p {output.pdf} -b {wildcards.bins}"
